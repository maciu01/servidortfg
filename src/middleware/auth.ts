/**
 * Middleware de autenticación.
 * @module middleware/auth
 */

import * as jwt from "jsonwebtoken";

/**
 * Middleware para verificar si el usuario está autenticado.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 * @param {NextFunction} next - La función para pasar al siguiente middleware.
 */
const auth = async (req:any, res:any, next:any) => {
    try {
        //   get the token from the authorization header
        const token = await req.headers.authorization.split(" ")[1];
    
        //check if the token matches the supposed origin
        const decodedToken = await jwt.verify(token, "RANDOM-TOKEN");
    
        // retrieve the user details of the logged in user
        const user = await decodedToken;
    
        // pass the user down to the endpoints here
        req.user = user;
    
        // pass down functionality to the endpoint
        next();
        
      } catch (error) {
        res.status(401).json({
          error: new Error("Invalid request!"),
        });
      }
}

export default auth;