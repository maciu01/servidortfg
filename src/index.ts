import "reflect-metadata"
import app from "./app"
import { AppDataSource} from "./db"

async function main() {
    try {
        const port = 3000;
        await AppDataSource.initialize()
        console.log('Database conected')
        app.listen(port)
    } catch (error) {
        console.error(error);
    }
    
}

main();


