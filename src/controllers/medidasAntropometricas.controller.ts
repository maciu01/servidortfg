/**
 * Controlador para manejar las operaciones de informes de antropometría.
 * @module controllers/medidasAntropometricasController
 */

import { Request, Response } from "express";
import { MedidasAntropometricas } from "../entities/MedidasAntropometricas";
import { Informe } from "../entities/Informe";
import { User } from "../entities/User";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un informe de tipo antropometría.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createMedidas = async (req: Request, res: Response) => {
    try {
        const{ talla, peso, perimetroAbdominal1, perimetroAbdominal2, perimetroAbdominal3, grasa,
            nivelGrasa, musculo, imc, bd, bi, pd, pi} = req.body;
        
        const medidasAntropometricas = new MedidasAntropometricas();
        medidasAntropometricas.talla = talla;
        medidasAntropometricas.peso = peso;
        medidasAntropometricas.perimetroAbdominal1 = perimetroAbdominal1;
        medidasAntropometricas.perimetroAbdominal2 = perimetroAbdominal2;
        medidasAntropometricas.perimetroAbdominal3 = perimetroAbdominal3;
        medidasAntropometricas.grasa = grasa;
        medidasAntropometricas.nivelGrasa = nivelGrasa;
        medidasAntropometricas.musculo = musculo;
        medidasAntropometricas.imc = imc;
        medidasAntropometricas.bd = bd;
        medidasAntropometricas.bi = bi;
        medidasAntropometricas.pd = pd;
        medidasAntropometricas.pi = pi;
        

        await medidasAntropometricas.save();

        const { idPaciente, idUser } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(idPaciente)});
        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        const user = await User.findOneBy({id: parseInt(idUser)});
        if(!user) return res.status(404).json({message: 'User does not exists'});

        const informe = new Informe();  
        informe.paciente = paciente;
        informe.user = user;
        informe.medidasAntropometricas = medidasAntropometricas;

        await informe.save();

        return res.json(medidasAntropometricas);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve un informe de tipo antropometría por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getMedidas = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const medidasAntropometricas = await MedidasAntropometricas.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(medidasAntropometricas);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina un informe de tipo antropometría por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteMedidas = async (req:Request, res:Response) => {
    try {

        try {
            const {codigo} = req.params;
    
            const result = await MedidasAntropometricas.delete({codigo: parseInt(codigo)});
            
            if(result.affected === 0){
                return res.status(404).json({message: 'Medidas Antropometricas not found'});
            }
        
            return res.sendStatus(204);  
    
        } catch (error) {
    
            if(error instanceof Error){
                return res.status(500).json({message: error.message});
            }
    
        } 
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Actualiza un informe de tipo antropometría por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateMedidas = async (req:Request, res: Response) => {
    try {
        const { codigo } = req.params;

        const medidasAntropometricas = await MedidasAntropometricas.findOneBy({codigo: parseInt(codigo)});

        if(!medidasAntropometricas) return res.status(404).json({message: 'Medidas Antropometricas does not exists'});

        await MedidasAntropometricas.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};