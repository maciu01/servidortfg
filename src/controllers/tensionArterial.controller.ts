/**
 * Controlador para manejar las operaciones de informes de tensión arterial.
 * @module controllers/tensionArterialController
 */

import { Request, Response } from "express";
import { TensionArterial } from "../entities/TensionArterial";
import { Informe } from "../entities/Informe";
import { User } from "../entities/User";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un informe de tipo tensión arterial.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createTension = async (req: Request, res: Response) => {
    try {
        const{ pmm, pas, pad } = req.body;
        
        const tensionArterial = new TensionArterial();
        tensionArterial.pmm = pmm;
        tensionArterial.pas = pas;
        tensionArterial.pad = pad;

        await tensionArterial.save();

        const { idPaciente, idUser } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(idPaciente)});
        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        const user = await User.findOneBy({id: parseInt(idUser)});
        if(!user) return res.status(404).json({message: 'User does not exists'});

        const informe = new Informe();  
        informe.paciente = paciente;
        informe.user = user;
        informe.tensionArterial = tensionArterial;

        await informe.save();

        return res.json(tensionArterial);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve un informe de tipo tensión arterial por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getTension = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const tensionArterial = await TensionArterial.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(tensionArterial);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina un informe de tipo tensión arterial por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteTension = async (req:Request, res:Response) => {
    try {

        try {
            const {codigo} = req.params;
    
            const result = await TensionArterial.delete({codigo: parseInt(codigo)});
            
            if(result.affected === 0){
                return res.status(404).json({message: 'Tension Arterial not found'});
            }
        
            return res.sendStatus(204);  
    
        } catch (error) {
    
            if(error instanceof Error){
                return res.status(500).json({message: error.message});
            }
    
        } 
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Modifica un informe de tipo tensión arterial por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateTension = async (req:Request, res: Response) => {
    try {
        const { codigo } = req.params;

        const tension = await TensionArterial.findOneBy({codigo: parseInt(codigo)});

        if(!tension) return res.status(404).json({message: 'Tension Arterial does not exists'});

        await TensionArterial.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};