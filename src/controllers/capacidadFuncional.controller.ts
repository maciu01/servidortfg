/**
 * Controlador para manejar las operaciones de informes de capacidad funcional.
 * @module controllers/capacidadFuncionalController
 */

import { Request, Response } from "express";
import { CapacidadFuncional } from "../entities/CapacidadFuncional";
import { Informe } from "../entities/Informe";
import { User } from "../entities/User";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un informe de tipo capacidad funcional
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createCapacidad = async (req: Request, res: Response) => {
    try {
        const{ tiempoPiesJuntos, tiempoPieAdelante, tiempoPiesFila,
            tiempoDesplazamiento, tiempoPruebaFuerza, velocidad, 
            potencia, distancia, metrosAtras1, metrosAtras2, metrosAtras3, pasosCuadrados1, pasosCuadrados2, pasosCuadrados3,
            falloEjecucion} = req.body;
        
        const capacidadFuncional = new CapacidadFuncional();
        capacidadFuncional.tiempoPiesJuntos = tiempoPiesJuntos;
        capacidadFuncional.tiempoPieAdelante = tiempoPieAdelante;
        capacidadFuncional.tiempoPiesFila = tiempoPiesFila;
        capacidadFuncional.tiempoDesplazamiento = tiempoDesplazamiento;
        capacidadFuncional.tiempoPruebaFuerza = tiempoPruebaFuerza;
        capacidadFuncional.velocidad = velocidad;
        capacidadFuncional.potencia = potencia;
        capacidadFuncional.distancia = distancia;
        capacidadFuncional.metrosAtras1 = metrosAtras1;
        capacidadFuncional.metrosAtras2 = metrosAtras2;
        capacidadFuncional.metrosAtras3 = metrosAtras3;
        capacidadFuncional.pasosCuadrados1 = pasosCuadrados1;
        capacidadFuncional.pasosCuadrados2 = pasosCuadrados2;
        capacidadFuncional.pasosCuadrados3 = pasosCuadrados3;
        capacidadFuncional.falloEjecucion = falloEjecucion;

        await capacidadFuncional.save();

        const { idPaciente, idUser } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(idPaciente)});
        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        const user = await User.findOneBy({id: parseInt(idUser)});
        if(!user) return res.status(404).json({message: 'User does not exists'});

        const informe = new Informe();  
        informe.paciente = paciente;
        informe.user = user;
        informe.capacidadFuncional = capacidadFuncional;

        await informe.save();

        return res.json(capacidadFuncional);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve un informe de tipo capacidad funcional por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getCapacidad = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const capacidadFuncional = await CapacidadFuncional.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(capacidadFuncional);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina un informe de tipo capacidad funcional por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteCapacidad = async (req:Request, res:Response) => {
    try {

        try {
            const {codigo} = req.params;
    
            const result = await CapacidadFuncional.delete({codigo: parseInt(codigo)});
            
            if(result.affected === 0){
                return res.status(404).json({message: 'Capacidad funcional not found'});
            }
        
            return res.sendStatus(204);  
    
        } catch (error) {
    
            if(error instanceof Error){
                return res.status(500).json({message: error.message});
            }
    
        } 
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Actualiza un informe de tipo capacidad funcional por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateCapacidad = async (req:Request, res: Response) => {
    try {
        const { codigo } = req.params;

        const capacidadFuncional = await CapacidadFuncional.findOneBy({codigo: parseInt(codigo)});

        if(!capacidadFuncional) return res.status(404).json({message: 'Capacidad does not exists'});

        await CapacidadFuncional.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};