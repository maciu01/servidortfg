/**
 * Controlador para manejar las operaciones de pacientes.
 * @module controllers/pacienteController
 */

import { Request, Response } from "express";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createPaciente = async (req: Request, res: Response) => {
    try {
        const { codHistorico, firstname, lastname, telefono, dateBirth, viven, grupo, nivelEducacional, sexo} = req.body;

        const paciente = new Paciente();
        paciente.codHistorico = codHistorico;
        paciente.firstname = firstname;
        paciente.lastname = lastname;
        paciente.telefono = telefono;
        paciente.dateBirth = dateBirth;
        paciente.viven = viven;
        paciente.grupo = grupo;
        paciente.nivelEducacional = nivelEducacional;
        paciente.sexo = sexo;

        await paciente.save();

        return res.json(paciente);
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Devuelve una lista con todos los pacientes.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getPacientes = async (req: Request, res: Response) => {
    try {
        const pacientes = await Paciente.find();

        return res.json(pacientes);

    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve los pacientes activos en el sistema.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getPacientesUser = async (req: Request, res: Response) => {
    try {
        const pacientes = await Paciente.find({
            where: { active: true },
            relations: ['entrevistas', 'informes']
        });

        return res.json(pacientes);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Actualiza un paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updatePacientes = async (req: Request, res: Response) => {
    try {

        const { id } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(req.params.id)});

        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        await Paciente.update({id: parseInt(id)}, req.body);

        return res.sendStatus(204);

    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }

};

/**
 * Desactiva un paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deletePaciente = async (req: Request, res: Response) => {
    try {
        const { codHistorico } = req.params;

        // Buscar al usuario por ID
        const paciente = await Paciente.findOneBy({ codHistorico: codHistorico });

        if (!paciente) {
            return res.status(404).json({ message: 'Paciente does not exist' });
        }

        paciente.active = false;
        await Paciente.save(paciente);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Activa un paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const activatePaciente = async (req: Request, res: Response) => {
    try {
        const { codHistorico } = req.params;

        // Buscar al usuario por ID
        const paciente = await Paciente.findOneBy({ codHistorico: codHistorico });

        if (!paciente) {
            return res.status(404).json({ message: 'Paciente does not exist' });
        }

        paciente.active = true;
        await Paciente.save(paciente);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Devuelve un paciente por código histórico.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getPaciente = async (req:Request, res:Response) => {
    try {
        const { codHistorico } = req.params;

        const paciente = await Paciente.findOneBy({codHistorico: codHistorico});
    
        return res.json(paciente);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Devuelve un paciente por ID.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getPacienteInforme = async (req:Request, res:Response) => {
    try {
        const { id } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(id)});
    
        return res.json(paciente);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};
