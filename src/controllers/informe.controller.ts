/**
 * Controlador para manejar las operaciones de informes
 * @module controllers/informeController
 */

import { Request, Response } from "express";
import { Informe } from "../entities/Informe";
import { Paciente } from "../entities/Paciente";

/**
 * Devuelve una lista con todos los informes.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getInformes = async (req: Request, res: Response) => {
    try {
        const informes = await Informe.find();

        return res.json(informes);

    } catch (error) {

        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }

    }
};

/**
 * Devuelve una lista con todos los informes de un paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getInformesPaciente =async (req:Request, res: Response) => {
    try {
        const { id } = req.params;

        const pacienteExistente = await Paciente.findOneBy({id: parseInt(id)});

        if(!pacienteExistente) return res.status(404).json({message: 'Paciente does not exists'});
        
        const informes = await Informe.find({where:{ pacienteId:pacienteExistente.id}});

        if (informes.length === 0) {
            return res.status(404).json({ message: 'No se han encontrado informes' });
        }

        return res.json(informes);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};