/**
 * Controlador para manejar las operaciones de informes de otros.
 * @module controllers/otrosController
 */

import { Request, Response } from "express";
import { Otros } from "../entities/Otros";
import { Informe } from "../entities/Informe";
import { User } from "../entities/User";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un informe de tipo otros.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createOtros = async (req: Request, res: Response) => {
    try {
        const{ lawtonBrody, moca } = req.body;
        
        const otros = new Otros();
        otros.lawtonBrody = lawtonBrody;
        otros.moca = moca;

        await otros.save();

        const { idPaciente, idUser } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(idPaciente)});
        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        const user = await User.findOneBy({id: parseInt(idUser)});
        if(!user) return res.status(404).json({message: 'User does not exists'});

        const informe = new Informe();  
        informe.paciente = paciente;
        informe.user = user;
        informe.otros = otros;

        await informe.save();

        return res.json(otros);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve un informe de tipo otros por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getOtros = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const otros = await Otros.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(otros);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina un informe de tipo otros por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteOtros = async (req:Request, res:Response) => {
    try {

        try {
            const {codigo} = req.params;
    
            const result = await Otros.delete({codigo: parseInt(codigo)});
            
            if(result.affected === 0){
                return res.status(404).json({message: 'Otros not found'});
            }
        
            return res.sendStatus(204);  
    
        } catch (error) {
    
            if(error instanceof Error){
                return res.status(500).json({message: error.message});
            }
    
        } 
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Actualiza un informe de tipo otros por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateOtros = async (req:Request, res: Response) => {
    try {
        const { codigo } = req.params;

        const otros = await Otros.findOneBy({codigo: parseInt(codigo)});

        if(!otros) return res.status(404).json({message: 'Otros does not exists'});

        await Otros.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};