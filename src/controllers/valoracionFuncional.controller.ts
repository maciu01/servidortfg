/**
 * Controlador para manejar las operaciones de informes de valoración funcional.
 * @module controllers/valoracionFuncionalController
 */

import { Request, Response } from "express";
import { ValoracionFuncional } from "../entities/ValoracionFuncional";
import { Informe } from "../entities/Informe";
import { User } from "../entities/User";
import { Paciente } from "../entities/Paciente";

/**
 * Crea un informe de tipo valoración funcional.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createValoracion = async (req: Request, res: Response) => {
    try {
        const{ mediaDerecha, mediaIzquierda, dsDerecha, dsIzquierda } = req.body;
        
        const valoracionFuncional = new ValoracionFuncional();
        valoracionFuncional.mediaDerecha = mediaDerecha;
        valoracionFuncional.mediaIzquierda = mediaIzquierda;
        valoracionFuncional.dsDerecha = dsDerecha;
        valoracionFuncional.dsIzquierda = dsIzquierda;

        await valoracionFuncional.save();

        const { idPaciente, idUser } = req.params;

        const paciente = await Paciente.findOneBy({id: parseInt(idPaciente)});
        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});

        const user = await User.findOneBy({id: parseInt(idUser)});
        if(!user) return res.status(404).json({message: 'User does not exists'});

        const informe = new Informe();  
        informe.paciente = paciente;
        informe.user = user;
        informe.valoracionFuncional = valoracionFuncional;

        await informe.save();

        return res.json(valoracionFuncional);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }
};

/**
 * Devuelve un informe de tipo valoración funcional por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getValoracion = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const valoracionFuncional = await ValoracionFuncional.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(valoracionFuncional);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina un informe de tipo valoración funcional por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteValoracion = async (req:Request, res:Response) => {
    try {

        try {
            const {codigo} = req.params;
    
            const result = await ValoracionFuncional.delete({codigo: parseInt(codigo)});
            
            if(result.affected === 0){
                return res.status(404).json({message: 'Valoracion Funcional not found'});
            }
        
            return res.sendStatus(204);  
    
        } catch (error) {
    
            if(error instanceof Error){
                return res.status(500).json({message: error.message});
            }
    
        } 
        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Modifica un informe de tipo valoración funcional por código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateValoracion = async (req:Request, res: Response) => {
    try {
        const { codigo } = req.params;

        const valoracionFuncional = await ValoracionFuncional.findOneBy({codigo: parseInt(codigo)});

        if(!valoracionFuncional) return res.status(404).json({message: 'Valoracion Funcional does not exists'});

        await ValoracionFuncional.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};