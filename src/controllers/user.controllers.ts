/**
 * Controlador para manejar las operaciones de usuarios.
 * @module controllers/userController
 */

import { Request, Response } from "express";
import { User } from "../entities/User";
import * as jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { error } from "console";

/**
 * Crea un usuario.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const register = async (req: Request, res: Response) => {
    try {
        const { username, firstname, lastname, email, password, profesional, is_admin } = req.body;

        const existingUserByMail =  await User.findOneBy({ email: email });
        if (existingUserByMail) return res.json({ message: "Exists this user" });

        const existingUserByUsername = await User.findOneBy({ username: username });
        if (existingUserByUsername) return res.json({ message: "Exists this user" });


        bcrypt.hash(password, 10)
            .then((hashedPassword) => {
                const user = new User();
                user.username = username;
                user.firstname = firstname;
                user.lastname = lastname;
                user.email = email;
                user.password = hashedPassword;
                user.profesional = profesional;
                user.is_admin = is_admin;

                user.save();

                console.log(user);

                return res.json(user);
            })
            .catch((e) => {
                res.status(500).send({
                    message: "Password was not hashed successfully",
                    e,
                });
            });

    } catch (error) {

        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }

    }
};

/**
 * Crea un token para un usuario.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const login = async (req: Request, res: Response) => {
    try {
        const { username, password } = req.body;

        const user = await User.findOneBy({ username: username });

        if (!user) return res.status(404).json({ message: 'User does not exists' });

        if(user.active === false){
            res.status(200).send({message: "Tu usuario está desactivado"});
        }

        bcrypt.compare(password, user.password)
            .then((passwordCheck) => {
                if (!passwordCheck) {
                    return res.status(400).send({
                        message: "Password does not match",
                        error,
                    });
                }

                const token = jwt.sign(
                    {
                        userId: user.id,
                        userEmail: user.email,
                    },
                    "RANDOM-TOKEN",
                    { expiresIn: "24h" }
                );

                res.status(200).send({
                    message: "Login Successful",
                    id: user.id,
                    email: user.email,
                    is_admin: user.is_admin,
                    token,
                });
            })
            .catch((error) => {
                res.status(400).send({
                    message: "Passwords does not match",
                    error,
                });
            });

    } catch (error) {

        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }

    }
};

/**
 * Devuelve una lista con todos los usuarios.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getUsers = async (req: Request, res: Response) => {
    try {
        const users = await User.find({ where: { is_admin: false } });

        return res.json(users);

    } catch (error) {

        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }

    }
};

/**
 * Actualiza un usuario por ID.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        const user = await User.findOneBy({ id: parseInt(req.params.id) });

        if (!user) return res.status(404).json({ message: 'User does not exist' });

        // Comprueba si la contraseña está presente en el cuerpo de la solicitud
        if (req.body.password) {
            // Encripta la nueva contraseña antes de actualizarla
            bcrypt.hash(req.body.password, 10)
                .then(async (hashedPassword) => {
                    // Actualiza el usuario con la nueva contraseña encriptada
                    await User.update({ id: parseInt(id) }, { ...req.body, password: hashedPassword });
                    return res.sendStatus(204);
                })
                .catch((e) => {
                    return res.status(500).send({
                        message: "Password was not hashed successfully",
                        error: e,
                    });
                });
        } else {
            // Si no hay contraseña en el cuerpo de la solicitud, simplemente actualiza los otros campos
            await User.update({ id: parseInt(id) }, req.body);
            return res.sendStatus(204);
        }

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Desactiva un usuario por ID.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        // Buscar al usuario por ID
        const user = await User.findOneBy({ id: parseInt(id) });

        if (!user) {
            return res.status(404).json({ message: 'User does not exist' });
        }

        user.active = false;
        await User.save(user);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Activa un usuario por ID.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const activateUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        // Buscar al usuario por ID
        const user = await User.findOneBy({ id: parseInt(id) });

        if (!user) {
            return res.status(404).json({ message: 'User does not exist' });
        }

        user.active = true;
        await User.save(user);

        return res.sendStatus(204);

    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Devuelve un usuario por ID.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getUser = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;

        const user = await User.findOneBy({ id: parseInt(id) });

        return res.json(user);

    } catch (error) {

        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }

    }
};