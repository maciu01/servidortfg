/**
 * Controlador para manejar las operaciones de entrevistas.
 * @module controllers/entrevistaController
 */

import { Request, Response } from "express";
import { Entrevista } from "../entities/Entrevista";
import { Paciente } from "../entities/Paciente";


/**
 * Crea una entrevista.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const createEntrevista = async (req:Request, res:Response) => {
    try {
        const{ fumador, tiempoFumador, aniosDeshabilitacion, alcohol, caidas12Meses, numCaidas, ayudaLocomocion, 
            protesisArticulares, protesisRodilla, protesisCadera, protesisHombro, protesisManoDedos, protesisOtros,
            vertigos, mareosAndar, cardiovascular, hipertension, insuficienciaCardiaca, accCerebrovascular, anginaPecho,
            infarto, trombosis, otrosCardiovascular, respiratorio, asma, epoc, otrosRespiratorio, genitoUrinario, infeccionUrinaria,
            calculosRenales, insuficienciaRenal, incotinenciaUrinaria, prostata, otrosGenitoUrinario, endocrino, diabetes,
            hipotiroidismo, hipercolesterolemia, osteoporosis, otrosEndocrino, digestivo, herniaHiato, reflujo, pancreatitis,
            enfermedadHepatica, diverticulitis, otrosDigestivo, musculoEsqueletico, pieTobillo, rodilla, cadera,lumbar,
            dorsal, cervical, cabeza, hombro, codo, mano_munieca, nervioso, depresion, ansiedad, esclerosisMultiple, 
            parkinson, alzheimer, deterioroCognitivo, neuropatiaPeriferica, otrosNervioso, sentidos, hipoacusia,
            tinitus, perdidaVision, datosInteres, medicacion, antihipertensivos, antidiabeticos, estatinas, anticoagulantes, 
            tiroides, diuretico, analgesicos, antidepresivos, ansioliticos, antiosteoporoticos, inhalador, 
            protectorEstomago, otrosMedicacion, venredi, terapiaOcupacional } = req.body;

        const entrevista = new Entrevista();
        entrevista.fumador = fumador;
        entrevista.tiempoFumador = tiempoFumador;
        entrevista.aniosDeshabilitacion = aniosDeshabilitacion;
        entrevista.alcohol = alcohol;
        entrevista.caidas12Meses = caidas12Meses;
        entrevista.numCaidas = numCaidas;
        entrevista.ayudaLocomocion = ayudaLocomocion;
        entrevista.protesisArticulares = protesisArticulares;
        entrevista.protesisRodilla = protesisRodilla;
        entrevista.protesisCadera = protesisCadera;
        entrevista.protesisHombro = protesisHombro;
        entrevista.protesisManoDedos = protesisManoDedos;
        entrevista.protesisOtros = protesisOtros;
        entrevista.vertigos = vertigos;
        entrevista.mareosAndar = mareosAndar;
        entrevista.cardiovascular = cardiovascular;
        entrevista.hipertension = hipertension;
        entrevista.insuficienciaCardiaca = insuficienciaCardiaca;
        entrevista.accCerebrovascular = accCerebrovascular;
        entrevista.anginaPecho = anginaPecho;
        entrevista.infarto = infarto;
        entrevista.trombosis = trombosis;
        entrevista.otrosCardiovascular = otrosCardiovascular;
        entrevista.respiratorio = respiratorio;
        entrevista.asma = asma;
        entrevista.epoc = epoc;
        entrevista.otrosRespiratorio = otrosRespiratorio;
        entrevista.genitoUrinario = genitoUrinario;
        entrevista.infeccionUrinaria = infeccionUrinaria;
        entrevista.calculosRenales = calculosRenales;
        entrevista.insuficienciaRenal = insuficienciaRenal;
        entrevista.incotinenciaUrinaria = incotinenciaUrinaria;
        entrevista.prostata = prostata;
        entrevista.otrosGenitoUrinario = otrosGenitoUrinario;
        entrevista.endocrino = endocrino;
        entrevista.diabetes = diabetes;
        entrevista.hipotiroidismo = hipotiroidismo;
        entrevista.hipercolesterolemia = hipercolesterolemia;
        entrevista.osteoporosis = osteoporosis;
        entrevista.otrosEndocrino = otrosEndocrino;
        entrevista.digestivo = digestivo;
        entrevista.herniaHiato = herniaHiato;
        entrevista.reflujo = reflujo;
        entrevista.pancreatitis = pancreatitis;
        entrevista.enfermedadHepatica = enfermedadHepatica;
        entrevista.diverticulitis = diverticulitis;
        entrevista.otrosDigestivo = otrosDigestivo;
        entrevista.musculoEsqueletico = musculoEsqueletico;
        entrevista.pieTobillo = pieTobillo;
        entrevista.rodilla = rodilla;
        entrevista.cadera = cadera;
        entrevista.lumbar = lumbar;
        entrevista.dorsal = dorsal;
        entrevista.cervical = cervical;
        entrevista.cabeza = cabeza;
        entrevista.hombro = hombro;
        entrevista.codo = codo;
        entrevista.mano_munieca = mano_munieca;
        entrevista.nervioso = nervioso;
        entrevista.depresion = depresion;
        entrevista.ansiedad = ansiedad;
        entrevista.esclerosisMultiple = esclerosisMultiple;
        entrevista.parkinson = parkinson;
        entrevista.alzheimer = alzheimer;
        entrevista.deterioroCognitivo = deterioroCognitivo;
        entrevista.neuropatiaPeriferica = neuropatiaPeriferica;
        entrevista.otrosNervioso = otrosNervioso;
        entrevista.sentidos = sentidos;
        entrevista.hipoacusia = hipoacusia;
        entrevista.tinitus = tinitus;
        entrevista.perdidaVision = perdidaVision;
        entrevista.datosInteres = datosInteres;
        entrevista.medicacion = medicacion;
        entrevista.antihipertensivos = antihipertensivos;
        entrevista.antidiabeticos = antidiabeticos;
        entrevista.estatinas = estatinas;
        entrevista.anticoagulantes = anticoagulantes;
        entrevista.tiroides = tiroides;
        entrevista.diuretico = diuretico;
        entrevista.analgesicos = analgesicos;
        entrevista.antidepresivos = antidepresivos;
        entrevista.ansioliticos = ansioliticos;
        entrevista.antiosteoporoticos = antiosteoporoticos;
        entrevista.inhalador = inhalador;
        entrevista.protectorEstomago = protectorEstomago;
        entrevista.otrosMedicacion = otrosMedicacion;
        entrevista.venredi = venredi;
        entrevista.terapiaOcupacional = terapiaOcupacional;

        const { id } = req.params;
        const paciente = await Paciente.findOneBy({id: parseInt(id)});

        if(!paciente) return res.status(404).json({message: 'Paciente does not exists'});
        entrevista.paciente = paciente;

        let toDay = new Date();
        let fecha_nac = paciente.dateBirth;

        let anioNacimiento = fecha_nac.getFullYear();
        let anioActual = toDay.getFullYear();

        let edad = anioActual - anioNacimiento;

        entrevista.edad = edad;

        await Entrevista.save(entrevista);

        const entrevistas = await Entrevista.find();

        return res.json(entrevistas);

        
    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }
    }

};

/**
 * Devuelve una entrevista por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getEntrevista = async (req:Request, res:Response) => {
    try {
        const { codigo } = req.params;

        const entrevista = await Entrevista.findOneBy({codigo: parseInt(codigo)});
    
        return res.json(entrevista);

    } catch (error) {

        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Devuelve un conjunto de entrevistas por el ID del paciente.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getEntrevistasPaciente = async (req:Request, res: Response) => {
    try {
        const { id } = req.params;

        const pacienteExistente = await Paciente.findOneBy({id: parseInt(id)});

        if(!pacienteExistente) return res.status(404).json({message: 'Paciente does not exists'});
        
        const entrevistas = await Entrevista.find({where:{ pacienteId:pacienteExistente.id} });


        if (entrevistas.length === 0) {
            return res.status(404).json({ message: 'No se han encontrado entrevistas' });
        }

        return res.json(entrevistas);

        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Devuelve una lista con todas las entrevistas.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const getEntrevistas = async (req: Request, res: Response) => {
    try {
        const entrevistas = await Entrevista.find();

        return res.json(entrevistas);

    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Actualiza una entrevista por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const updateEntrevista = async (req:Request, res:Response) => {
    try {

        const { codigo } = req.params;

        const entrevista = await Entrevista.findOneBy({codigo: parseInt(codigo)});

        if(!entrevista) return res.status(404).json({message: 'Entrevista does not exists'});

        await Entrevista.update({codigo: parseInt(codigo)}, req.body);

        return res.sendStatus(204);
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};

/**
 * Elimina una entrevista por su código.
 * @param {Request} req - La solicitud de Express.
 * @param {Response} res - La respuesta de Express.
 */
export const deleteEntrevista = async (req:Request, res:Response) => {
    try {

        const {codigo} = req.params;

        const result = await Entrevista.delete({codigo: parseInt(codigo)});
        
        if(result.affected === 0){
            return res.status(404).json({message: 'Entrevista not found'});
        }
    
        return res.sendStatus(204);  
        
    } catch (error) {
        
        if(error instanceof Error){
            return res.status(500).json({message: error.message});
        }

    }
};