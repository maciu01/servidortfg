/**
 * Representa un informe de medidas antropométricas.
 * @typedef {Object} MedidasAntropometricas
 * @property {number} codigo - El código del informe.
 * @property {number} talla - Talla de la persona en metros.
 * @property {number} peso - Peso de la persona en kilogramos.
 * @property {number} perimetroAbdominal1 - Primer medida del perímetro abdominal en centímetros.
 * @property {number} perimetroAbdominal2 - Segunda medida del perímetro abdominal en centímetros.
 * @property {number} perimetroAbdominal3 - Tercer medida del perímetro abdominal en centímetros.
 * @property {number} mediaPerimetroAbdominal - Media de las medidas del perímetro abdominal en centímetros.
 * @property {number} grasa - Porcentaje de grasa corporal.
 * @property {number} nivelGrasa - Nivel de grasa.
 * @property {number} musculo - Porcentaje de masa muscular.
 * @property {number} imc - Índice de masa corporal.
 * @property {number} bd - Masa del brazo derecho en kilogramos.
 * @property {number} bi - Masa del brazo izquierdo en kilogramos.
 * @property {number} pd - Masa de la pierna derecha en kilogramos.
 * @property {number} pi - Masa de la pierna izquierda en kilogramos.
 * @property {number} pesoBrazosPiernas - Masa total de los brazos y piernas en kilogramos.
 * @property {number} smi - Índice de masa muscular apendicular (MMA).
 * @property {Date} createdAt - Fecha de creación del informe.
 * @property {Informe} informe - El informe asociado a la valoración funcional.
 */

import { BaseEntity, Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate } from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class MedidasAntropometricas extends BaseEntity {
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column({ type: 'float', scale: 2 })
    talla: number;

    @Column({ type: 'float', scale: 2 })
    peso: number;

    @Column({ type: 'float', scale: 1 })
    perimetroAbdominal1: number;

    @Column({ type: 'float', scale: 1 })
    perimetroAbdominal2: number;

    @Column({ type: 'float', scale: 1 })
    perimetroAbdominal3: number;

    @Column({ type: 'float', scale: 1 })
    mediaPerimetroAbdominal: number;

    @Column({ type: 'float', scale: 1 })
    grasa: number;

    @Column()
    nivelGrasa: number;

    @Column({ type: 'float', scale: 1 })
    musculo: number;

    @Column({ type: 'float', scale: 2 })
    imc: number;

    @Column({ type: 'float', scale: 2 })
    bd: number;

    @Column({ type: 'float', scale: 2 })
    bi: number;

    @Column({ type: 'float', scale: 2 })
    pd: number;

    @Column({ type: 'float', scale: 2 })
    pi: number;

    @Column({ type: 'float', scale: 2 })
    pesoBrazosPiernas: number;

    @Column({ type: 'float', scale: 2 })
    smi: number;    //Masa muscular apendicular

    @CreateDateColumn()
    createdAt: Date;

    @OneToOne(() => Informe, (informe) => informe.medidasAntropometricas)
    informe: Informe;

    @BeforeInsert()
    @BeforeUpdate()
    updateMediaPerimetroAbdominal() {
        const media = ((this.perimetroAbdominal1+this.perimetroAbdominal2+this.perimetroAbdominal3)/3.0);
        this.mediaPerimetroAbdominal = parseFloat(media.toFixed(1));
    }


    @BeforeInsert()
    @BeforeUpdate()
    updatePesoBrazosYPiernas() {
        const bdNum = this.bd;
        const biNum = this.bi;
        const pdNum = this.pd;
        const piNum = this.pi;

        const peso = (this.bd+this.bi+this.pd+this.pi);
        this.pesoBrazosPiernas = peso;
    }

    @BeforeInsert()
    @BeforeUpdate()
    updateSmi() {
        const smi = ((this.bd+this.bi+this.pd+this.pi)/(this.talla*this.talla));
        this.smi = parseFloat(smi.toFixed(1));
    }
}