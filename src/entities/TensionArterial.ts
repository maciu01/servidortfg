/**
 * Representa un registro de tensión arterial.
 * @typedef {Object} TensionArterial
 * @property {number} codigo - El identificador único del registro de tensión arterial.
 * @property {number} pmm - La presión media.
 * @property {number} pas - La presión arterial sistólica.
 * @property {number} pad - La presión arterial diastólica.
 * @property {Date} createdAt - Fecha de creación del registro.
 * @property {Informe} informe - Informe asociado al registro de tensión arterial.
 */

import {BaseEntity, Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn} from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class TensionArterial extends BaseEntity{
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column()
    pmm: number;

    @Column()
    pas: number;

    @Column()
    pad: number;

    @CreateDateColumn()
    createdAt: Date;

    @OneToOne(() => Informe, (informe) => informe.tensionArterial)
    informe: Informe;
}