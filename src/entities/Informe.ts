/**
 * Representa un informe de tipo capacidad funcional.
 * @typedef {Object} Informe
 * @property {number} id - El ID del informe.
 * @property {number} otrosId - El ID del informe de tipo otros.
 * @property {number} medidasId - El ID del informe de tipo antropometría.
 * @property {number} valoracionId - El ID del informe de tipo valoración funcional.
 * @property {number} capacidadId - El ID del informe de tipo capacidad funcional.
 * @property {number} tensionId - El ID del informe de tipo tensión arterial.
 * @property {number} pacienteId - El ID del paciente asociado.
 * @property {number} userId - El ID del usuario asociado.
 * @property {Date} createdAt - La fecha de creación del informe.
 * @property {Date} updatedAt - La fecha de modificación del informe.
 */

import {BaseEntity,  Entity, PrimaryGeneratedColumn, ManyToOne, OneToOne, JoinColumn, Column, CreateDateColumn, UpdateDateColumn} from 'typeorm'
import { Paciente } from './Paciente';
import { Otros } from './Otros';
import { MedidasAntropometricas } from './MedidasAntropometricas';
import { ValoracionFuncional } from './ValoracionFuncional';
import { CapacidadFuncional } from './CapacidadFuncional';
import { TensionArterial } from './TensionArterial';
import { User } from './User';

@Entity()
export class Informe extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    otrosId: number;

    @Column({nullable: true})
    medidasId: number;

    @Column({nullable: true})
    valoracionId: number;

    @Column({nullable: true})
    capacidadId: number;

    @Column({nullable: true})
    tensionId: number;

    @OneToOne(() => Otros, {
        onDelete:"CASCADE", onUpdate:'CASCADE'})
    @JoinColumn({name: "otrosId"})
    otros: Otros;

    @OneToOne(() => MedidasAntropometricas, {
        onDelete:"CASCADE", onUpdate:'CASCADE'})
    @JoinColumn({name: "medidasId"})
    medidasAntropometricas: MedidasAntropometricas;

    @OneToOne(() => ValoracionFuncional, {
        onDelete:"CASCADE", onUpdate:'CASCADE'})
    @JoinColumn({name: "valoracionId"})
    valoracionFuncional: ValoracionFuncional;

    @OneToOne(() => CapacidadFuncional, {
        onDelete:"CASCADE", onUpdate:'CASCADE'})
    @JoinColumn({name: "capacidadId"})
    capacidadFuncional: CapacidadFuncional;

    @OneToOne(() => TensionArterial, {
        onDelete:"CASCADE", onUpdate:'CASCADE'})
    @JoinColumn({name: "tensionId"})
    tensionArterial: TensionArterial;

    @Column()
    pacienteId: number;

    @ManyToOne(() => Paciente, (paciente) => paciente.informes)
    paciente: Paciente;

    @Column()
    userId: number;

    @ManyToOne(() => User, (user) => user.informes)
    user: User;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}