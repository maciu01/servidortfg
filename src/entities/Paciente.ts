/**
 * Representa un paciente.
 * @typedef {Object} Paciente
 * @property {number} id - El identificador único del paciente.
 * @property {string} codHistorico - El código histórico del paciente.
 * @property {string} firstname - El nombre del paciente.
 * @property {string} lastname - El apellido del paciente.
 * @property {string} telefono - El número de teléfono del paciente.
 * @property {boolean} active - Indica si el paciente está activo.
 * @property {Date} dateBirth - La fecha de nacimiento del paciente.
 * @property {number} viven - Estado de vida (0, 1, 2).
 * @property {number} grupo - Grupo al que pertenece el paciente (0-13).
 * @property {number} nivelEducacional - Nivel educacional (0-4).
 * @property {number} sexo - Sexo del paciente (1, 2).
 * @property {Date} createdAt - Fecha de creación del registro.
 * @property {Date} updatedAt - Fecha de la última actualización del registro.
 * @property {Entrevista[]} entrevistas - Lista de entrevistas asociadas al paciente.
 * @property {Informe[]} informes - Lista de informes asociados al paciente.
 */


import {BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, OneToMany} from 'typeorm'
import { Entrevista } from './Entrevista';
import { Informe } from './Informe';

@Entity()
export class Paciente extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique: true})
    codHistorico: string;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column()
    telefono: string;

    @Column({default: true})
    active: boolean;

    @Column()
    dateBirth: Date;

    @Column({type: 'enum', enum: [0, 1, 2]})
    viven: number;

    @Column({type: 'enum',  enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]})
    grupo: number;
    
    @Column({type: 'enum', enum: [0, 1, 2, 3, 4]})
    nivelEducacional: number;

    @Column({type: 'enum', enum: [1, 2]})
    sexo: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Entrevista, (entrevista) => entrevista.paciente)
    entrevistas: Entrevista[];

    @OneToMany(() => Informe, (informe) => informe.paciente)
    informes: Informe[];
}