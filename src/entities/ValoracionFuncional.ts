/**
 * Representa una valoración funcional.
 * @typedef {Object} ValoracionFuncional
 * @property {number} codigo - El identificador único de la valoración funcional.
 * @property {number} mediaDerecha - Media de la valoración funcional del lado derecho.
 * @property {number} mediaIzquierda - Media de la valoración funcional del lado izquierdo.
 * @property {number} dsDerecha - Desviación estándar de la valoración funcional del lado derecho.
 * @property {number} dsIzquierda - Desviación estándar de la valoración funcional del lado izquierdo.
 * @property {Date} createdAt - Fecha de creación de la valoración funcional.
 * @property {Informe} informe - El informe asociado a la valoración funcional.
 */

import {BaseEntity, Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn} from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class ValoracionFuncional extends BaseEntity{
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column({type: 'float', scale: 2})
    mediaDerecha: number;

    @Column({type: 'float', scale: 2})
    mediaIzquierda: number;

    @Column({type: 'float', scale: 2})
    dsDerecha: number;

    @Column({type: 'float', scale: 2})
    dsIzquierda: number;

    @CreateDateColumn()
    createdAt: Date;

    @OneToOne(() => Informe, (informe) => informe.valoracionFuncional)
    informe: Informe;
}