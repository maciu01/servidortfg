/**
 * Representa un usuario del sistema.
 * @typedef {Object} User
 * @property {number} id - El identificador único del usuario.
 * @property {string} username - El nombre de usuario, único en el sistema.
 * @property {string} firstname - El primer nombre del usuario.
 * @property {string} lastname - El apellido del usuario.
 * @property {boolean} active - Indica si el usuario está activo.
 * @property {string} email - El correo electrónico del usuario.
 * @property {string} password - La contraseña del usuario.
 * @property {number} profesional - Nivel profesional del usuario (0: Nivel 0, 1: Nivel 1, 2: Nivel 2).
 * @property {boolean} is_admin - Indica si el usuario tiene privilegios de administrador.
 * @property {Date} createdAt - Fecha de creación del usuario.
 * @property {Date} updatedAt - Fecha de la última actualización del usuario.
 * @property {Informe[]} informes - Lista de informes asociados al usuario.
 */

import {BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class User extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique:true})
    username: string;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column({default: true})
    active: boolean;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({type: 'enum', enum: [0, 1, 2]})
    profesional: number;

    @Column({default: false})
    is_admin: boolean;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Informe, (informe) => informe.user)
    informes: Informe[];
}