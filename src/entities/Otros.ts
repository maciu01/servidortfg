/**
 * Representa otro tipo de evaluación.
 * @typedef {Object} Otros
 * @property {number} codigo - El identificador único del registro de evaluación.
 * @property {number} lawtonBrody - Puntuación en la evaluación de Lawton Brody.
 * @property {number} moca - Puntuación en la evaluación MoCA (Montreal Cognitive Assessment).
 * @property {Date} createdAt - Fecha de creación del registro.
 * @property {Informe} informe - Informe asociado al registro de evaluación.
 */

import {BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, OneToOne} from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class Otros extends BaseEntity{
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column()
    lawtonBrody: number;

    @Column()
    moca: number;

    @CreateDateColumn()
    createdAt: Date;

    @OneToOne(() => Informe, (informe) => informe.otros)
    informe: Informe;
}