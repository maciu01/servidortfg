/**
 * Representa una entrevista realizada a un paciente.
 * @typedef {Object} Entrevista
 * @property {number} codigo - El código de la entrevista.
 * @property {number} edad - La edad del paciente.
 * @property {number} fumador - Estado de fumador (1: nunca, 2: fumador, 3: ex-fumador).
 * @property {number} [tiempoFumador] - Tiempo que ha sido fumador.
 * @property {number} [aniosDeshabilitacion] - Años de deshabilitación por fumar.
 * @property {number} alcohol - Estado de consumo de alcohol (1: nunca, 2: consumidor, 3: ex-consumidor).
 * @property {number} [tiempoAlcohol] - Tiempo que ha consumido alcohol.
 * @property {number} [aniosDeshabilitacionAlcohol] - Años de deshabilitación por alcohol.
 * @property {boolean} caidas12Meses - Indica si ha tenido caídas en los últimos 12 meses.
 * @property {number} [numCaidas] - Número de caídas en los últimos 12 meses.
 * @property {boolean} ayudaLocomocion - Indica si necesita ayuda para la locomoción.
 * @property {boolean} protesisArticulares - Indica si tiene prótesis articulares.
 * @property {boolean} protesisRodilla - Indica si tiene prótesis en la rodilla.
 * @property {boolean} protesisCadera - Indica si tiene prótesis en la cadera.
 * @property {boolean} protesisHombro - Indica si tiene prótesis en el hombro.
 * @property {boolean} protesisManoDedos - Indica si tiene prótesis en la mano o dedos.
 * @property {string} [protesisOtros] - Otros tipos de prótesis que pueda tener.
 * @property {boolean} vertigos - Indica si sufre de vértigos.
 * @property {boolean} mareosAndar - Indica si sufre mareos al andar.
 * @property {boolean} cardiovascular - Indica si tiene enfermedades cardiovasculares.
 * @property {boolean} hipertension - Indica si tiene hipertensión.
 * @property {boolean} insuficienciaCardiaca - Indica si tiene insuficiencia cardíaca.
 * @property {boolean} accCerebrovascular - Indica si ha tenido un accidente cerebrovascular.
 * @property {boolean} anginaPecho - Indica si sufre de angina de pecho.
 * @property {boolean} infarto - Indica si ha tenido un infarto.
 * @property {boolean} trombosis - Indica si ha tenido una trombosis.
 * @property {string} [otrosCardiovascular] - Otros problemas cardiovasculares.
 * @property {boolean} respiratorio - Indica si tiene enfermedades respiratorias.
 * @property {boolean} asma - Indica si tiene asma.
 * @property {boolean} epoc - Indica si tiene EPOC.
 * @property {string} [otrosRespiratorio] - Otros problemas respiratorios.
 * @property {boolean} genitoUrinario - Indica si tiene enfermedades genitourinarias.
 * @property {boolean} infeccionUrinaria - Indica si tiene infecciones urinarias.
 * @property {boolean} calculosRenales - Indica si tiene cálculos renales.
 * @property {boolean} insuficienciaRenal - Indica si tiene insuficiencia renal.
 * @property {boolean} incotinenciaUrinaria - Indica si tiene incontinencia urinaria.
 * @property {boolean} prostata - Indica si tiene problemas de próstata.
 * @property {string} [otrosGenitoUrinario] - Otros problemas genitourinarios.
 * @property {boolean} endocrino - Indica si tiene enfermedades endocrinas.
 * @property {boolean} diabetes - Indica si tiene diabetes.
 * @property {boolean} hipotiroidismo - Indica si tiene hipotiroidismo.
 * @property {boolean} hipercolesterolemia - Indica si tiene hipercolesterolemia.
 * @property {boolean} osteoporosis - Indica si tiene osteoporosis.
 * @property {string} [otrosEndocrino] - Otros problemas endocrinos.
 * @property {boolean} digestivo - Indica si tiene enfermedades digestivas.
 * @property {boolean} herniaHiato - Indica si tiene hernia de hiato.
 * @property {boolean} reflujo - Indica si tiene reflujo.
 * @property {boolean} pancreatitis - Indica si tiene pancreatitis.
 * @property {boolean} enfermedadHepatica - Indica si tiene enfermedades hepáticas.
 * @property {boolean} diverticulitis - Indica si tiene diverticulitis.
 * @property {string} [otrosDigestivo] - Otros problemas digestivos.
 * @property {boolean} musculoEsqueletico - Indica si tiene problemas musculoesqueléticos.
 * @property {boolean} pieTobillo - Indica si tiene problemas en pie/tobillo.
 * @property {boolean} rodilla - Indica si tiene problemas en la rodilla.
 * @property {boolean} cadera - Indica si tiene problemas en la cadera.
 * @property {boolean} lumbar - Indica si tiene problemas lumbares.
 * @property {boolean} dorsal - Indica si tiene problemas dorsales.
 * @property {boolean} cervical - Indica si tiene problemas cervicales.
 * @property {boolean} cabeza - Indica si tiene problemas en la cabeza.
 * @property {boolean} hombro - Indica si tiene problemas en el hombro.
 * @property {boolean} codo - Indica si tiene problemas en el codo.
 * @property {boolean} mano_munieca - Indica si tiene problemas en mano/muñeca.
 * @property {boolean} nervioso - Indica si tiene problemas en el sistema nervioso.
 * @property {boolean} depresion - Indica si tiene depresión.
 * @property {boolean} ansiedad - Indica si tiene ansiedad.
 * @property {boolean} esclerosisMultiple - Indica si tiene esclerosis múltiple.
 * @property {boolean} parkinson - Indica si tiene Parkinson.
 * @property {boolean} alzheimer - Indica si tiene Alzheimer.
 * @property {boolean} deterioroCognitivo - Indica si tiene deterioro cognitivo.
 * @property {boolean} neuropatiaPeriferica - Indica si tiene neuropatía periférica.
 * @property {string} [otrosNervioso] - Otros problemas del sistema nervioso.
 * @property {boolean} sentidos - Indica si tiene problemas sensoriales.
 * @property {boolean} hipoacusia - Indica si tiene hipoacusia.
 * @property {boolean} tinitus - Indica si tiene tinitus.
 * @property {boolean} perdidaVision - Indica si tiene pérdida de visión.
 * @property {string} [datosInteres] - Otros datos de interés.
 * @property {boolean} medicacion - Indica si toma medicación.
 * @property {boolean} antihipertensivos - Indica si toma antihipertensivos.
 * @property {boolean} antidiabeticos - Indica si toma antidiabéticos.
 * @property {boolean} estatinas - Indica si toma estatinas.
 * @property {boolean} anticoagulantes - Indica si toma anticoagulantes.
 * @property {boolean} tiroides - Indica si toma medicación para el tiroides.
 * @property {boolean} diuretico - Indica si toma diuréticos.
 * @property {boolean} analgesicos - Indica si toma analgésicos.
 * @property {boolean} antidepresivos - Indica si toma antidepresivos.
 * @property {boolean} ansioliticos - Indica si toma ansiolíticos.
 * @property {boolean} antiosteoporoticos - Indica si toma medicamentos antiosteoporóticos.
 * @property {boolean} inhalador - Indica si usa inhalador.
 * @property {boolean} protectorEstomago - Indica si toma protector de estómago.
 * @property {string} [otrosMedicacion] - Otras medicaciones.
 * @property {Date} createdAt - Fecha de creación de la entrevista.
 * @property {boolean} venredi - Indica si asiste a la actividad Venredi.
 * @property {boolean} terapiaOcupacional - Indica si asiste a terapia ocupacional.
 * @property {number} pacienteId - El ID del paciente asociado.
 */


import {BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, ManyToOne} from 'typeorm'
import { Paciente } from './Paciente';

@Entity()
export class Entrevista extends BaseEntity{
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column()
    edad: number;

    @Column({type: 'enum', enum: [1, 2, 3]})
    fumador: number;

    @Column({ nullable: true })
    tiempoFumador: number;

    @Column({ nullable: true })
    aniosDeshabilitacion: number;

    @Column({type: 'enum', enum: [1, 2, 3]})
    alcohol: number;

    @Column({ nullable: true })
    tiempoAlcohol: number;

    @Column({ nullable: true })
    aniosDeshabilitacionAlcohol: number;

    @Column()
    caidas12Meses: boolean;

    @Column({ nullable: true })
    numCaidas: number;

    @Column()
    ayudaLocomocion: boolean;

    @Column()
    protesisArticulares: boolean;

    @Column()
    protesisRodilla: boolean;

    @Column()
    protesisCadera: boolean;

    @Column()
    protesisHombro: boolean;

    @Column()
    protesisManoDedos: boolean;

    @Column({ nullable: true })
    protesisOtros: string;

    @Column()
    vertigos: boolean;

    @Column()
    mareosAndar: boolean;

    @Column()
    cardiovascular: boolean;

    @Column()
    hipertension: boolean;

    @Column()
    insuficienciaCardiaca: boolean;

    @Column()
    accCerebrovascular: boolean;

    @Column()
    anginaPecho: boolean;

    @Column()
    infarto: boolean;

    @Column()
    trombosis: boolean;

    @Column({ nullable: true })
    otrosCardiovascular: string;

    @Column()
    respiratorio: boolean;

    @Column()
    asma: boolean;

    @Column()
    epoc: boolean;

    @Column({ nullable: true })
    otrosRespiratorio: string;

    @Column()
    genitoUrinario: boolean;

    @Column()
    infeccionUrinaria: boolean;

    @Column()
    calculosRenales: boolean;

    @Column()
    insuficienciaRenal: boolean;

    @Column()
    incotinenciaUrinaria: boolean;

    @Column()
    prostata: boolean;

    @Column({ nullable: true })
    otrosGenitoUrinario: string;

    @Column()
    endocrino: boolean;

    @Column()
    diabetes: boolean;

    @Column()
    hipotiroidismo: boolean;

    @Column()
    hipercolesterolemia: boolean;

    @Column()
    osteoporosis: boolean;

    @Column({ nullable: true })
    otrosEndocrino: string;

    @Column()
    digestivo: boolean;

    @Column()
    herniaHiato: boolean;

    @Column()
    reflujo: boolean;

    @Column()
    pancreatitis: boolean;

    @Column()
    enfermedadHepatica: boolean;

    @Column()
    diverticulitis: boolean;

    @Column({ nullable: true })
    otrosDigestivo: string;

    @Column()
    musculoEsqueletico: boolean;

    @Column()
    pieTobillo: boolean;

    @Column()
    rodilla: boolean;

    @Column()
    cadera: boolean;

    @Column()
    lumbar: boolean;

    @Column()
    dorsal: boolean;

    @Column()
    cervical: boolean;

    @Column()
    cabeza: boolean;

    @Column()
    hombro: boolean;

    @Column()
    codo: boolean;

    @Column()
    mano_munieca: boolean;

    @Column()
    nervioso: boolean;

    @Column()
    depresion: boolean;

    @Column()
    ansiedad: boolean;

    @Column()
    esclerosisMultiple: boolean;

    @Column()
    parkinson: boolean;

    @Column()
    alzheimer: boolean;

    @Column()
    deterioroCognitivo: boolean;

    @Column()
    neuropatiaPeriferica: boolean;

    @Column({ nullable: true })
    otrosNervioso: string;

    @Column()
    sentidos: boolean;

    @Column()
    hipoacusia: boolean;

    @Column()
    tinitus: boolean;

    @Column()
    perdidaVision: boolean;

    @Column({ nullable: true })
    datosInteres: string;

    @Column()
    medicacion: boolean;

    @Column()
    antihipertensivos: boolean;

    @Column()
    antidiabeticos: boolean;

    @Column()
    estatinas: boolean;

    @Column()
    anticoagulantes: boolean;

    @Column()
    tiroides: boolean;

    @Column()
    diuretico: boolean;

    @Column()
    analgesicos: boolean;

    @Column()
    antidepresivos: boolean;

    @Column()
    ansioliticos: boolean;

    @Column()
    antiosteoporoticos: boolean;

    @Column()
    inhalador: boolean;

    @Column()
    protectorEstomago: boolean;

    @Column({ nullable: true })
    otrosMedicacion: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column()
    venredi: boolean;

    @Column()
    terapiaOcupacional: boolean;

    @Column()
    pacienteId:number;

    @ManyToOne(() => Paciente, (paciente) => paciente.entrevistas)
    paciente: Paciente;
}