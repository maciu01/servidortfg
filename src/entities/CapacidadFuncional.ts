/**
 * Representa un informe de tipo capacidad funcional.
 * @typedef {Object} CapacidadFuncional
 * @property {number} codigo - El código del informe.
 * @property {number} tiempoPiesJuntos - Tiempo de la prueba pies juntos.
 * @property {number} puntosPiesJuntos - Puntos de la prueba pies juntos.
 * @property {number} tiempoPieAdelante - Tiempo de la prueba pie adelante.
 * @property {number} puntosPieAdelante - Puntos de la prueba pie adelante.
 * @property {number} tiempoPiesFila - Tiempo de la prueba pies en fila.
 * @property {number} puntosPieFila - Puntos de la prueba pies en fila.
 * @property {number} puntosEquilibrio - Puntos totales de las pruebas de equilibrio
 * @property {number} tiempoDesplazamiento - Tiempo de la prueba de desplazamiento.
 * @property {number} puntosDesplazamiento - Puntos de la prueba de desplazamiento.
 * @property {number} tiempoPruebaFuerza - Tiempo de la prueba de fuerza.
 * @property {number} puntosTiempoFuerza - Puntos de la prueba de fuerza.
 * @property {number} puntosSppb - Puntos acumulados en las pruebas de equilibrio, desplazamiento y fuerza.
 * @property {number} velocidad - Tiempo de la prueba de velocidad.
 * @property {number} potencia - Tiempo de la prueba de potencia.
 * @property {number} distancia - Tiempo de la prueba de distancia.
 * @property {number} metrosAtras1 - Primer tiempo de la prueba metros hacia atrás.
 * @property {number} metrosAtras2 - Segundo tiempo de la prueba metros hacia atrás.
 * @property {number} metrosAtras3 - Tercer tiempo de la prueba metros hacia atrás.
 * @property {number} mediaMetrosAtras - Media de los tiempo de la prueba metros hacia atrás.
 * @property {number} pasosCuadrados1 - Primer tiempo de la prueba pasos cuadrados.
 * @property {number} pasosCuadrados2 - Segundo tiempo de la prueba pasos cuadrados.
 * @property {number} pasosCuadrados3 - Tercer tiempo de la prueba pasos cuadrados.
 * @property {number} mediaPasosCuadrados - Media de los tiempo de la prueba pasos hacia atrás.
 * @property {boolean} falloEjecucion - Indica si la ejecución de la prueba pasos hacia atrás ha sido buena.
 * @property {Informe} informe - El informe asociado a la capacidad funcional.
 * @property {Date} createdAt - Fecha de creación del informe
 */

import { BaseEntity, Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate } from 'typeorm'
import { Informe } from './Informe';

@Entity()
export class CapacidadFuncional extends BaseEntity {
    @PrimaryGeneratedColumn()
    codigo: number;

    @Column({ type: 'float', scale: 2 })
    tiempoPiesJuntos: number;

    @Column()
    puntosPiesJuntos: number;

    @Column({ type: 'float', scale: 2 })
    tiempoPieAdelante: number;

    @Column()
    puntosPieAdelante: number;

    @Column({ type: 'float', scale: 2 })
    tiempoPiesFila: number;

    @Column()
    puntosPieFila: number;

    @Column()
    puntosEquilibrio: number;

    @Column({ type: 'float', scale: 2 })
    tiempoDesplazamiento: number;

    @Column()
    puntosDesplazamiento: number;

    @Column({ type: 'float', scale: 2 })
    tiempoPruebaFuerza: number;

    @Column()
    puntosTiempoFuerza: number;

    @Column()
    puntosSppb: number;

    @Column({ type: 'float', scale: 2 })
    velocidad: number;

    @Column({ type: 'float', scale: 2 })
    potencia: number;

    @Column({ type: 'float', scale: 2 })
    distancia: number;

    @Column({ type: 'float', scale: 2 })
    metrosAtras1: number;

    @Column({ type: 'float', scale: 2 })
    metrosAtras2: number;

    @Column({ type: 'float', scale: 2 })
    metrosAtras3: number;

    @Column({ type: 'float', scale: 2 })
    mediaMetrosAtras: number;

    @Column({ type: 'float', scale: 2 })
    pasosCuadrados1: number;

    @Column({ type: 'float', scale: 2 })
    pasosCuadrados2: number;

    @Column({ type: 'float', scale: 2 })
    pasosCuadrados3: number;

    @Column({ type: 'float', scale: 2 })
    mediaPasosCuadrados: number;

    @Column()
    falloEjecucion: boolean;

    @OneToOne(() => Informe, (informe) => informe.capacidadFuncional)
    informe: Informe;

    @CreateDateColumn()
    createdAt: Date;

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosPiesJuntos() {
        if (this.tiempoPiesJuntos >= 10) {
            this.puntosPiesJuntos = 1;
        } else {
            this.puntosPiesJuntos = 0;
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosPieAdelante() {
        if (this.tiempoPieAdelante >= 10) {
            this.puntosPieAdelante = 1;
        } else {
            this.puntosPieAdelante = 0;
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosPieFila() {
        if (this.tiempoPiesFila < 3) {
            this.puntosPieFila = 0;
        } else if (this.tiempoPiesFila >= 3 && this.tiempoPiesFila < 10) {
            this.puntosPieFila = 1;
        } else {
            this.puntosPieFila = 2;
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosEquilibrio() {
        this.puntosEquilibrio = this.puntosPiesJuntos + this.puntosPieAdelante + this.puntosPieFila;
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosDesplazamiento() {
        if (this.tiempoDesplazamiento === 0.0) {
            this.puntosDesplazamiento = 0;
        } else if (this.tiempoDesplazamiento < 4.82) {
            this.puntosDesplazamiento = 4;
        } else if (this.tiempoDesplazamiento >= 4.82 && this.tiempoDesplazamiento < 6.21) {
            this.puntosDesplazamiento = 3;
        } else if (this.tiempoDesplazamiento >= 6.21 && this.tiempoDesplazamiento <= 8.7) {
            this.puntosDesplazamiento = 2;
        } else {
            this.puntosDesplazamiento = 1;
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePruebaFuerza() {
        if (this.tiempoPruebaFuerza === 0.0 || this.tiempoPruebaFuerza >= 60) {
            this.puntosTiempoFuerza = 0;
        } else if (this.tiempoPruebaFuerza < 11.20) {
            this.puntosTiempoFuerza = 4;
        } else if (this.tiempoPruebaFuerza >= 11.20 && this.tiempoPruebaFuerza < 13.70) {
            this.puntosTiempoFuerza = 3;
        } else if (this.tiempoPruebaFuerza >= 13.70 && this.tiempoPruebaFuerza < 16.70) {
            this.puntosTiempoFuerza = 2;
        } else {
            this.puntosTiempoFuerza = 1;
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    updatePuntosSppb() {
        this.puntosSppb = this.puntosEquilibrio + this.puntosDesplazamiento + this.puntosTiempoFuerza;
    }

    @BeforeInsert()
    @BeforeUpdate()
    updateMediaPasosAtras(){
        this.mediaMetrosAtras = ((this.metrosAtras1+this.metrosAtras2+this.metrosAtras3) / 3.0);
    }

    @BeforeInsert()
    @BeforeUpdate()
    updateMediaPasosCuadrados() {
        this.mediaPasosCuadrados = Math.max(this.pasosCuadrados1, this.pasosCuadrados2, this.pasosCuadrados3);
    }
}