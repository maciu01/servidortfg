import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import userRoutes from './routes/user.routes'
import pacienteRoutes from './routes/paciente.routes'
import entrevistaRoutes from './routes/entrevista.routes'
import informeRoutes from './routes/informe.routes'
import otrosRoutes from './routes/otros.routes'
import tensionArterial from './routes/tensionArterial.routes'
import valoracionFuncional from './routes/valoracionFuncional.routes'
import capacidadFuncional from './routes/capacidadFuncional.routes'
import medidasAntropometricas from './routes/medidasAntropometricas.routes'

const app = express()

app.use(morgan('dev'))
app.use(cors())
app.use(express.json());

app.use(userRoutes);
app.use(pacienteRoutes);
app.use(entrevistaRoutes);
app.use(informeRoutes);
app.use(otrosRoutes);
app.use(tensionArterial);
app.use(valoracionFuncional);
app.use(capacidadFuncional);
app.use(medidasAntropometricas);

export default app