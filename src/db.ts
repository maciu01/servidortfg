import { DataSource } from 'typeorm';
import { User } from './entities/User';
import { Paciente } from './entities/Paciente';
import { Entrevista } from './entities/Entrevista';
import { Informe } from './entities/Informe';
import { Otros } from './entities/Otros';
import { MedidasAntropometricas } from './entities/MedidasAntropometricas';
import { ValoracionFuncional } from './entities/ValoracionFuncional';
import { TensionArterial } from './entities/TensionArterial';
import { CapacidadFuncional } from './entities/CapacidadFuncional';


export const AppDataSource = new DataSource({
    type: 'postgres',
    host: 'localhost',
    username: 'postgres',
    password: 'Movistar07',
    port: 5432,
    database: 'sociosalud',
    entities: [User, Paciente, Entrevista, Informe, Otros, 
    MedidasAntropometricas, ValoracionFuncional, TensionArterial,
    CapacidadFuncional],
    logging: true,
    synchronize: true
})