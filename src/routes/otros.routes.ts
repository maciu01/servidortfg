/**
 * Rutas para entidad Otros.
 * @module routes/otrosRoutes
 */

import { Router } from 'express';
import { createOtros, deleteOtros, getOtros, updateOtros } from '../controllers/otros.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /otros/:idPaciente/:idUser
 * @description Crea un registro de Otros para un paciente específico.
 * @param {string} idPaciente - ID del paciente al que pertenece el registro.
 * @param {string} idUser - ID del usuario que realiza la acción.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/otros/:idPaciente/:idUser', auth, createOtros);

/**
 * GET /otros/:codigo
 * @description Obtiene un registro de Otros por su código.
 * @param {string} codigo - Código único del registro de Otros.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/otros/:codigo', auth, getOtros);

/**
 * DELETE /otros/:codigo
 * @description Elimina un registro de Otros por su código.
 * @param {string} codigo - Código único del registro de Otros a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/otros/:codigo', auth, deleteOtros);

/**
 * PUT /otros/:codigo
 * @description Actualiza un registro de Otros por su código.
 * @param {string} codigo - Código único del registro de Otros a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/otros/:codigo', auth, updateOtros);

export default router;
