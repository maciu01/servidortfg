/**
 * Rutas para entidad ValoracionFuncional.
 * @module routes/valoracionFuncionalRoutes
 */

import { Router } from 'express';
import { createValoracion, deleteValoracion, getValoracion, updateValoracion } from '../controllers/valoracionFuncional.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /valoracion/:idPaciente/:idUser
 * @description Crea una nueva valoración funcional para un paciente por un usuario.
 * @param {string} idPaciente - ID del paciente para el cual se realiza la valoración.
 * @param {string} idUser - ID del usuario que realiza la valoración.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/valoracion/:idPaciente/:idUser', auth, createValoracion);

/**
 * GET /valoracion/:codigo
 * @description Obtiene una valoración funcional por su código.
 * @param {string} codigo - Código de la valoración funcional.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/valoracion/:codigo', auth, getValoracion);

/**
 * DELETE /valoracion/:codigo
 * @description Elimina una valoración funcional por su código.
 * @param {string} codigo - Código de la valoración funcional a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/valoracion/:codigo', auth, deleteValoracion);

/**
 * PUT /valoracion/:codigo
 * @description Actualiza una valoración funcional por su código.
 * @param {string} codigo - Código de la valoración funcional a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/valoracion/:codigo', auth, updateValoracion);

export default router;
