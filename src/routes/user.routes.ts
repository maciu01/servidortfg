/**
 * Rutas para entidad User.
 * @module routes/userRoutes
 */

import { Router } from 'express';
import { register, getUsers, updateUser, deleteUser, getUser, login, activateUser } from '../controllers/user.controllers';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /users/register
 * @description Registra un nuevo usuario.
 */
router.post('/users/register', register);

/**
 * POST /users/login
 * @description Permite que un usuario inicie sesión.
 */
router.post('/users/login', login);

/**
 * GET /users
 * @description Obtiene todos los usuarios.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/users', auth, getUsers);

/**
 * PUT /users/:id
 * @description Actualiza un usuario por su ID.
 * @param {string} id - ID del usuario a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/users/:id', auth, updateUser);

/**
 * PUT /users/delete/:id
 * @description Elimina lógicamente un usuario por su ID.
 * @param {string} id - ID del usuario a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/users/delete/:id', auth, deleteUser);

/**
 * PUT /users/activate/:id
 * @description Activa un usuario por su ID.
 * @param {string} id - ID del usuario a activar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/users/activate/:id', auth, activateUser);

/**
 * GET /users/:id
 * @description Obtiene un usuario por su ID.
 * @param {string} id - ID del usuario.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/users/:id', auth, getUser);

export default router;
