/**
 * Rutas de medidas antropométricas.
 * @module routes/medidasAntropometricasRoutes
 */

import { Router } from 'express';
import { createMedidas, deleteMedidas, getMedidas, updateMedidas } from '../controllers/medidasAntropometricas.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /medidas/:idPaciente/:idUser
 * @description Crea nuevas medidas antropométricas para un paciente.
 * @param {string} idPaciente - El ID del paciente al que pertenecen las medidas.
 * @param {string} idUser - El ID del usuario que realiza la creación.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/medidas/:idPaciente/:idUser', auth, createMedidas);

/**
 * GET /medidas/:codigo
 * @description Obtiene las medidas antropométricas por su código.
 * @param {string} codigo - El código único de las medidas antropométricas.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/medidas/:codigo', auth, getMedidas);

/**
 * DELETE /medidas/:codigo
 * @description Elimina las medidas antropométricas por su código.
 * @param {string} codigo - El código único de las medidas antropométricas a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/medidas/:codigo', auth, deleteMedidas);

/**
 * PUT /medidas/:codigo
 * @description Actualiza las medidas antropométricas por su código.
 * @param {string} codigo - El código único de las medidas antropométricas a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/medidas/:codigo', auth, updateMedidas);

export default router;
