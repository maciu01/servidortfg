/**
 * Rutas de entrevistas.
 * @module routes/entrevistaRoutes
 */

import { Router } from 'express';
import { createEntrevista, deleteEntrevista, getEntrevista, getEntrevistas, getEntrevistasPaciente, updateEntrevista } from '../controllers/entrevista.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /entrevistas/:id
 * @description Crea una nueva entrevista para un paciente específico.
 * @param {string} id - El ID del paciente asociado a la entrevista.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/entrevistas/:id', auth, createEntrevista);

/**
 * GET /entrevistas/:id
 * @description Obtiene todas las entrevistas de un paciente por su ID.
 * @param {string} id - El ID del paciente para filtrar las entrevistas.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/entrevistas/:id', auth, getEntrevistasPaciente);

/**
 * GET /entrevistas
 * @description Obtiene todas las entrevistas.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/entrevistas', auth, getEntrevistas);

/**
 * GET /entrevista/:codigo
 * @description Obtiene una entrevista por su código.
 * @param {string} codigo - El código de la entrevista a obtener.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/entrevista/:codigo', auth, getEntrevista);

/**
 * PUT /entrevistas/:codigo
 * @description Actualiza una entrevista por su código.
 * @param {string} codigo - El código de la entrevista a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/entrevistas/:codigo', auth, updateEntrevista);

/**
 * DELETE /entrevistas/:codigo
 * @description Elimina una entrevista por su código.
 * @param {string} codigo - El código de la entrevista a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/entrevistas/:codigo', auth, deleteEntrevista);

export default router;
