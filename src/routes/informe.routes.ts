/**
 * Rutas de informes.
 * @module routes/informeRoutes
 */

import { Router } from 'express';
import { getInformes, getInformesPaciente } from '../controllers/informe.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * GET /informes
 * @description Obtiene todos los informes disponibles.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/informes', auth, getInformes);

/**
 * GET /informes/:id
 * @description Obtiene los informes de un paciente por su ID.
 * @param {string} id - El ID del paciente para filtrar los informes.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/informes/:id', auth, getInformesPaciente);

export default router;
