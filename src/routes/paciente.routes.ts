/**
 * Rutas para entidad Paciente.
 * @module routes/pacienteRoutes
 */

import { Router } from 'express';
import { activatePaciente, createPaciente, deletePaciente, getPaciente, getPacienteInforme, getPacientes, getPacientesUser, updatePacientes } from '../controllers/paciente.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /pacientes
 * @description Crea un nuevo paciente.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/pacientes', auth, createPaciente);

/**
 * GET /pacientes
 * @description Obtiene todos los pacientes.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/pacientes', auth, getPacientes);

/**
 * GET /pacientes/actives
 * @description Obtiene todos los pacientes activos asociados a un usuario.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/pacientes/actives', auth, getPacientesUser);

/**
 * GET /pacientes/:codHistorico
 * @description Obtiene un paciente por su código histórico.
 * @param {string} codHistorico - Código histórico único del paciente.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/pacientes/:codHistorico', auth, getPaciente);

/**
 * GET /pacientes/informes/:id
 * @description Obtiene los informes asociados a un paciente por su ID.
 * @param {string} id - ID del paciente.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/pacientes/informes/:id', auth, getPacienteInforme);

/**
 * PUT /pacientes/:id
 * @description Actualiza un paciente por su ID.
 * @param {string} id - ID del paciente a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/pacientes/:id', auth, updatePacientes);

/**
 * PUT /pacientes/delete/:codHistorico
 * @description Elimina lógicamente un paciente por su código histórico.
 * @param {string} codHistorico - Código histórico único del paciente a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/pacientes/delete/:codHistorico', auth, deletePaciente);

/**
 * PUT /pacientes/activate/:codHistorico
 * @description Activa un paciente previamente desactivado por su código histórico.
 * @param {string} codHistorico - Código histórico único del paciente a activar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/pacientes/activate/:codHistorico', auth, activatePaciente);

export default router;
