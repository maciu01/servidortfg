/**
 * Rutas para entidad TensionArterial.
 * @module routes/tensionArterialRoutes
 */

import { Router } from 'express';
import { createTension, deleteTension, getTension, updateTension } from '../controllers/tensionArterial.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /tension/:idPaciente/:idUser
 * @description Crea un nuevo registro de tensión arterial para un paciente.
 * @param {string} idPaciente - ID del paciente al que se le registra la tensión arterial.
 * @param {string} idUser - ID del usuario que realiza el registro.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/tension/:idPaciente/:idUser', auth, createTension);

/**
 * GET /tension/:codigo
 * @description Obtiene un registro de tensión arterial por su código.
 * @param {string} codigo - Código único del registro de tensión arterial.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/tension/:codigo', auth, getTension);

/**
 * DELETE /tension/:codigo
 * @description Elimina un registro de tensión arterial por su código.
 * @param {string} codigo - Código único del registro de tensión arterial a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/tension/:codigo', auth, deleteTension);

/**
 * PUT /tension/:codigo
 * @description Actualiza un registro de tensión arterial por su código.
 * @param {string} codigo - Código único del registro de tensión arterial a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/tension/:codigo', auth, updateTension);

export default router;
