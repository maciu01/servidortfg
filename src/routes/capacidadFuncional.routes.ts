/**
 * Rutas de capacidad funcional.
 * @module routes/capacidadFuncionalRoutes
 */

import { Router } from 'express';
import { createCapacidad, deleteCapacidad, getCapacidad, updateCapacidad } from '../controllers/capacidadFuncional.controller';
import auth from '../middleware/auth';

const router = Router();

/**
 * POST /capacidad/:idPaciente/:idUser
 * @description Crea un nuevo registro de capacidad funcional para un paciente.
 * @param {string} idPaciente - El ID del paciente asociado.
 * @param {string} idUser - El ID del usuario que realiza la acción.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.post('/capacidad/:idPaciente/:idUser', auth, createCapacidad);

/**
 * GET /capacidad/:codigo
 * @description Obtiene un registro de capacidad funcional por su código.
 * @param {string} codigo - El código del registro de capacidad funcional.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.get('/capacidad/:codigo', auth, getCapacidad);

/**
 * DELETE /capacidad/:codigo
 * @description Elimina un registro de capacidad funcional por su código.
 * @param {string} codigo - El código del registro de capacidad funcional a eliminar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.delete('/capacidad/:codigo', auth, deleteCapacidad);

/**
 * PUT /capacidad/:codigo
 * @description Actualiza un registro de capacidad funcional por su código.
 * @param {string} codigo - El código del registro de capacidad funcional a actualizar.
 * @middleware auth - Middleware de autenticación requerido.
 */
router.put('/capacidad/:codigo', auth, updateCapacidad);

export default router;
